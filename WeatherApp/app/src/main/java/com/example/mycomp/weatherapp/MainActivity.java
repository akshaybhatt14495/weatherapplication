package com.example.mycomp.weatherapp;

import android.content.Intent;
import android.graphics.*;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.*;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.*;
import android.widget.*;
import java.io.*;
import java.net.*;

public class MainActivity extends AppCompatActivity {
    EditText country;
    String data;
    EditText city;
    WeatherDeatils d;
    Bitmap bitmap;
    ImageView img;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        country=(EditText)findViewById(R.id.editcountry);
        city=(EditText)findViewById(R.id.editcity);
        img =(ImageView)findViewById(R.id.imageview);
 }

    public static String removeSpace(String s)
    {
        for (int i=0;i<s.length();i++)
        {
            if (s.charAt(i)==' ')
            {
                String temp=s;
                s=temp.substring(0,i)+"%20"+temp.substring(i+1);


            }
        }
        Log.i("akshay",s);
        return s;
    }
    void updateWeather(View v)
    {
        String ci=removeSpace(city.getText().toString());
        String co=removeSpace(country.getText().toString());
        if (ci.equals(""))
        {
            Toast.makeText(this,"city is mandatory",Toast.LENGTH_SHORT).show();
            return;
        }
        if (isNetworkAvailable())
        {
            GetWeather gw=new GetWeather();
            gw.execute(ci,co);
        }
        else
        {
            Toast.makeText(this,"No Network Available",Toast.LENGTH_LONG).show();
        }
    }
    public boolean isNetworkAvailable() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(this.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnectedOrConnecting()) {
            return true;
        }
        else
        {
            return false;
        }
    }
    class GetWeather extends AsyncTask<String,Void,String>
    {

        @Override
        protected String doInBackground(String... params) {
            try
            {
                String result="";
                URL url=new URL("http://api.openweathermap.org/data/2.5/weather?q="+params[1]+","+params[0]+"&APPID=72aaf750d91bd2718adf8366ef901d00");
                HttpURLConnection con=(HttpURLConnection)url.openConnection();
                con.connect();
                BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
                String str = br.readLine();
                while (str != null) {
                    result += str;
                    str = br.readLine();
                }
                con.disconnect();
                Log.v("URL", String.valueOf(url));

                data=result;

                d=new WeatherDeatils(data);
                url=new URL("http://openweathermap.org/img/w/"+d.icon+".png");
                InputStream conn=url.openStream();
                bitmap= BitmapFactory.decodeStream(conn);
                conn.close();
                return result;
            }
            catch(Exception e)
            {
                return e+"";
            }

        }


        public void onPostExecute(String data)
        {


            TextView temp=(TextView)findViewById(R.id.temperature);
            temp.setText(d.temperature+" C");
            TextView des=(TextView)findViewById(R.id.descrption);
            des.setText(d.description);
            TextView loc=(TextView)findViewById(R.id.location);
            loc.setText("Location  Lon:"+d.longitute+"Lat:"+d.latitute);
            img=(ImageView)findViewById(R.id.imageview);
            img.setImageBitmap(bitmap);



        }


    }

    void getDetails(View v)
    {
        Intent i=new Intent(this,DetailActivity.class);
        i.putExtra("data",data);
        i.putExtra("bitmap",bitmap);
        startActivity(i);
    }
}

