package com.example.mycomp.weatherapp;

import android.util.JsonReader;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;


public class WeatherDeatils
{
    private final String LOG_TAG=getClass().getSimpleName();
    String latitute="Data not availabe";
    String longitute="Data not availabe";
    String icon="nothing to show";
    String country="Data not availabe";
    String city="Data not availabe";
    String sunset="Data not availabe";
    String sunrise="Data not availabe";
    String description="Data not availabe";
    String temperature="Data not availabe",temp_min="Data not availabe",temp_max="Data not availabe";
    String pressure="Data not availabe";
    String humidity="Data not availabe";

    String windspeed="Data not availabe",winddegree="Data not availabe";
    String excption="";
    WeatherDeatils(String data)
    {
        try
        {
            JSONObject obj=new JSONObject(data);

            try {
                JSONObject coordinate=obj.getJSONObject("coord");
                latitute=coordinate.getString("lat");
                longitute=coordinate.getString("lon");
            }
            catch(Exception e)
            {
                excption+=e+"\n";
            }

           try {
               JSONArray arr=obj.getJSONArray("weather");
               JSONObject weather=arr.getJSONObject(0);
               description=weather.getString("description");
               icon=weather.getString("icon");
           }
           catch(Exception e)
           {
               excption+=e+"\n";
           }
            try
            {
                JSONObject main=obj.getJSONObject("main");

                temperature=toCelsius(main.getString("temp"));

                pressure=main.getString("pressure");
                humidity=main.getString("humidity");
                temp_min=toCelsius(main.getString("temp_min"));
                temp_max=toCelsius(main.getString("temp_max"));
            }
            catch(Exception e)
            {
                excption+=e+"\n";
            }

            try
            {
                JSONObject wind=obj.getJSONObject("wind");
                winddegree=wind.getString("deg");
            }
            catch(Exception e)
            {
                excption+=e+"\n";
            }
            try
            {
                JSONObject wind=obj.getJSONObject("wind");
                windspeed=wind.getString("speed");

            }
            catch(Exception e)
            {
                excption+=e+"\n";
            }

            try {
                city=obj.getString("name");
            }
            catch(Exception e)
            {
                excption+=e+"\n";
            }

            try {
                JSONObject sys= obj.getJSONObject("sys");
                country=sys.getString("country");
                sunrise=sys.getString("sunrise");
                sunset=sys.getString("sunset");
            }
            catch(Exception e)
            {
                excption+=e+"\n";
            }





        }
        catch(Exception e)
        {
            Log.i("akshay",e+"");
            excption=e+""+"\n";
        }

    }

    public static String toCelsius(String temp)
    {
        Float tem=Float.parseFloat(temp);
        tem-=273;
        int a=(int)(tem/1);
        Log.i("Akshay",a+"");
        return ""+a;
    }
    @Override
    public String toString() {
        return excption+"\n"+country+"\n"+city+"\n"+latitute+"\n"+longitute+"\n"+temperature+"\n"+temp_max+"\n"+temp_min+"\n"+sunrise+"\n"+sunset+"\n"
                +"\n"+windspeed+"\n"+winddegree+"\n"+description+"\n"+humidity+"\n"+pressure+"\n"+"03n";
    }
}
