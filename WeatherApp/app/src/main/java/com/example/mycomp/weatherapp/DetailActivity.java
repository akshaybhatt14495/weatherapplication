package com.example.mycomp.weatherapp;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.InputStream;
import java.net.URL;

public class DetailActivity extends AppCompatActivity {

    String data;
    WeatherDeatils d;
    ImageView img;
    Bitmap bitmap;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        Intent i=getIntent();
        data=i.getStringExtra("data");


        try
        {
            d=new WeatherDeatils(data);
            TextView latitute=(TextView)findViewById(R.id.latitute);
            latitute.setText("");
            img=(ImageView)findViewById(R.id.imagenext);



            latitute.setText("City Found: \t" + d.city + "\n" +
                    "Country Found:\t" + d.country + "\n" +
                    "Latitute:\t" + d.latitute + "\n" +
                    "Logitute:\t" + d.longitute + "\n" +
                    "Description:\t" + d.description + "\n" +
                    "Max Temprature:\t" + d.temp_max + " celcius\n" +
                    "Min Temprature:\t" + d.temp_min + " celsius\n" +
                    "Present Temp:\t" + d.temperature + " celsius\n" +
                    "Humidity:\t" + d.humidity + "\n" +
                    "Wind Speed:\t" + d.windspeed + " m/sec\n" +
                    "Wind Degree:\t" + d.winddegree + "\n" +
                    "Pressure:\t" + d.pressure + "millibars\n" +
                    "Humidity:\t" + d.latitute + "\n"

            );


            if (!d.excption.equals(""))
                Toast.makeText(DetailActivity.this,d.excption,Toast.LENGTH_SHORT).show();




        }
        catch(Exception e)
        {
            TextView latitute=(TextView)findViewById(R.id.latitute);
            latitute.setText(e+"");
            Toast.makeText(DetailActivity.this,e+"",Toast.LENGTH_LONG).show();
        }

        if (isNetworkAvailable())
        {
            GetImage img=new GetImage();
            img.execute();

        }
        else
        {
            Toast.makeText(this,"No network available",Toast.LENGTH_SHORT).show();
        }
    }


    public boolean isNetworkAvailable() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(this.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        }
        else
        {
            return false;
        }
    }


    class GetImage extends AsyncTask<Void,Void,Bitmap>
    {



        @Override
        protected Bitmap doInBackground(Void... voids) {
            try
            {
                URL  url=new URL("http://openweathermap.org/img/w/"+d.icon+".png");
                InputStream is=url.openStream();
                bitmap = BitmapFactory.decodeStream(is);
                is.close();
                return bitmap;
            }
            catch (Exception e)
            {
                return null;
            }

        }

        @Override


        public void onPostExecute(Bitmap data)
        {
            img.setImageBitmap(data);
        }
    }
}
